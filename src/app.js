import createError from 'http-errors';
import express, { json, urlencoded } from 'express';
import logger from 'morgan';
import helmet from 'helmet';
import errorHandler from 'strong-error-handler';

import AWSService from './libs/awsService';
import MongoDbService from './libs/mongodbService';

import upload from './upload';
import { mongoDb, awsService as _awsService, env } from '../config';

/**
 * Connect to database
 */

const mongoDbSerice = new MongoDbService();
mongoDbSerice.connect(
    `mongodb://${mongoDb.host}/${mongoDb.name}`,
);

/**
 * Configure file storage service
 */

const awsService = new AWSService({
    accessKeyId: _awsService.accessKeyId,
    secretAccessKey: _awsService.secretAccessKey,
    bucketName: _awsService.bucketName,
});

/**
 * Set up third party middleware functions
 */

var app = express();

app.use(logger('dev'));
app.use(helmet());
app.use(json());
app.use(urlencoded({ extended: false }));

/**
 * API
 */

app.use('/api/upload', upload(mongoDbSerice, awsService));

/**
 * Static Files
 */

app.use(express.static('./public'));
app.use('/doc', express.static('./doc'));

/**
 * Error handling
 */

app.use(function(req, res, next) {
    // catch 404 and forward to error handler
    next(createError(404));
});

app.use(errorHandler({debug: env !== 'production'}));

export default app;
