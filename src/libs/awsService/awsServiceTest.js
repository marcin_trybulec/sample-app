import AWSService from './awsService';
import sinon from 'sinon';
import 'chai/register-should';

describe('AWSService', function() {
    describe('constructor', function() {
        it('Must be called with proper arguments', function() {
            (() => new AWSService()).should.throw();

            (() => new AWSService({ accessKeyId: 'test1' })).should.throw();

            (() =>
                new AWSService({
                    accessKeyId: 'test2',
                    secretAccessKey: 'test2',
                })).should.throw();

            (() =>
                new AWSService({
                    accessKeyId: 'test3',
                    secretAccessKey: 'test3',
                    bucketName: 'test3',
                })).should.not.throw();
        });

        it('Should store bucket name as a property', function() {
            const awsService = new AWSService({
                accessKeyId: 'test3',
                secretAccessKey: 'test3',
                bucketName: 'test3',
            });

            awsService.bucketName.should.equal('test3');
        });
    });

    describe('uploadFile', function() {
        it('Should call AWS.S3 internal upload method', function() {
            const uploadStub = sinon.stub(AWSService.prototype, 'upload');

            const awsService = new AWSService({
                accessKeyId: 'test3',
                secretAccessKey: 'test3',
                bucketName: 'test3',
            });

            const fileName = 'fileName';
            const file = 'FileBuffer';
            awsService.uploadFile(file, fileName);

            should.equal(uploadStub.called, true);

            uploadStub.getCall(0).args[0].should.deep.equal({
                Bucket: awsService.bucketName,
                Body: file,
                Key: fileName,
            });
        });
    });
});
