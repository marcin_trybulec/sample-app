import AWS from 'aws-sdk';
import { promisify } from 'util';

export default class AWSService extends AWS.S3 {
    constructor({ accessKeyId, secretAccessKey, bucketName }) {
        if (!accessKeyId || !secretAccessKey || !bucketName) throw new Error();
   
        super({ accessKeyId, secretAccessKey, bucketName });

        this.bucketName = bucketName;
    }

    uploadFile(file, filename) {
        const params = {
            Bucket: this.bucketName,
            Body: file,
            Key: filename,
        };

        return promisify(this.upload.bind(this))(params);
    }
}
