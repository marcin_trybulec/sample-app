import { Schema, model } from 'mongoose';

const fileSchema = new Schema({
    location: {
        type: String,
        required: true,
    },
});

const File = model('File', fileSchema);

export default File;
