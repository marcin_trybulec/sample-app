import 'chai/register-should';
const config = require('../../../config');

import MongoDbService from './index';
import File from './file';

let mongodbService = null;

describe('MongoDbService', function() {
    before(function(done) {
        mongodbService = new MongoDbService();

        const connection = mongodbService.connect(
            `mongodb://${config.mongoDb.host}/${config.mongoDb.name}`,
        );
        connection.once('open', done);
    });

    beforeEach(function() {
        return File.deleteMany();
    });

    it('Should save valid file to db', async function() {
        const file = {
            location: 'testLocation',
        };

        const savedDoc = await mongodbService.saveFile(file);
        savedDoc.should.has.a.property('_id');
        savedDoc.should.has.a.property('location').equal(file.location);

        const count = await File.estimatedDocumentCount();
        count.should.equal(1);
    });

    it('Should not save invalid file to db', async function() {
        const fileMissingReqFields = {
            name: 'testName',
        };

        await mongodbService
            .saveFile(fileMissingReqFields)
            .catch(async function(err) {
                const count = await File.estimatedDocumentCount();
                count.should.equal(0);
            });
    });

    after(function() {
        mongodbService.close();
    });
});
