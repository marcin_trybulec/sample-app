import File from './file';
import mongoose from 'mongoose';

export default class MongodDbService {
    connect = dbURL => {
        mongoose.connect(dbURL, { useNewUrlParser: true });

        this.connection = mongoose.connection;

        return mongoose.connection;
    };

    close = () => {
        this.connection.close();
    };

    saveFile = fileToSave => {
        const file = new File(fileToSave);

        return file.save();
    };
}
