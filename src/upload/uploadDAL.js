export default class UploadDAL {
    constructor(dbService) {
        this.dbService = dbService;
    }

    saveFileLocation(location) {
        return this.dbService.saveFile({ location });
    }
}
