const Jimp = require('jimp');

export const resizeImage = async (
    image,
    width = Jimp.AUTO,
    height = Jimp.AUTO,
) => {
    const jimpImg = await Jimp.read(image);

    return jimpImg
        .resize(parseInt(width), parseInt(height))
        .getBufferAsync(jimpImg._originalMime);
};
