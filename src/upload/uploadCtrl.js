import UploadDAL from './uploadDAL';
import { resizeImage } from './uploadService';

export default class UploadCtrl {
    constructor(dbService, fileStorageService) {
        this.db = new UploadDAL(dbService);
        this.fileStorage = fileStorageService;
    }

    uploadImage = async (req, res) => {
        const { buffer: image, originalname: name } = req.file;
        const { width, height } = req.query;

        const resizedImage = await resizeImage(image, width, height);

        const { Location: location } = await this.fileStorage.uploadFile(
            resizedImage,
            Date.now() + name,
        );

        const { id } = await this.db.saveFileLocation(location);

        res.set('Location', location);
        res.status(201).send({
            _id: id,
            location,
        });
    };
}
