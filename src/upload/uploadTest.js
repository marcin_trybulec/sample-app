import PNGImage from 'pngjs-image';
import { promisify } from 'util';
import 'chai/register-should';
import { resizeImage } from './uploadService';
import request from 'supertest';
import Jimp from 'jimp';

const config = require('../../config');

import UploadDAL from './uploadDAL';
import MongoDbService from '../libs/mongodbService';
import File from '../libs/mongodbService/file';

let mongodbService = null;
let uploadDAL = null;

import app from '../app';

describe('uploadService', function() {
    describe('resizeImage', function() {
        it('Should resize image', async function() {
            const image = PNGImage.createImage(100, 300);

            const newWidth = 250;
            const newHeight = 250;

            const resizedImage = await resizeImage(
                image.getImage(),
                newWidth,
                newHeight,
            );

            const resizedPNG = await promisify(PNGImage.loadImage)(
                resizedImage,
            );

            resizedPNG.getWidth().should.equal(newWidth);
            resizedPNG.getHeight().should.equal(newHeight);
        });
    });
});

describe('uploadDAL', function() {
    before(function(done) {
        mongodbService = new MongoDbService();
        uploadDAL = new UploadDAL(mongodbService);

        const connection = mongodbService.connect(
            `mongodb://${config.mongoDb.host}/${config.mongoDb.name}`,
        );
        connection.once('open', done);
    });

    beforeEach(function() {
        return File.deleteMany();
    });

    it('Should save image location', async function() {
        const location = 'testLocation';

        const savedDoc = await uploadDAL.saveFileLocation(location);
        savedDoc.should.has.a.property('location').equal(location);

        const count = await File.estimatedDocumentCount();
        count.should.equal(1);
    });

    after(function() {
        mongodbService.close();
    });
});

//FIXME: Doesn't work 
describe.skip('API', function() {
    describe('POST /api/upload', function() {
        it('Save file and respond with json', function(done) {
            new Jimp(120, 120, async function(err, img) {
                const buffer = await img.getBufferAsync(Jimp.MIME_JPEG)

                request(app)
                    .post('/api/upload')
                    .attach('image', buffer)
                    .end(function(err, res) {
                        if (err) return done(err);
                        done();
                    });
            });
        });
    });
});
