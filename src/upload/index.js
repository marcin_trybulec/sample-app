import api from './uploadAPI';

export default (dbService, fileStorageService) => {
    return api(dbService, fileStorageService);
};
