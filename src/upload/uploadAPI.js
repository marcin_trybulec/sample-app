import { Router } from 'express';
import multer from 'multer';
const upload = multer();

import { asyncHandler } from '../libs/helpers';
import UploadCtrl from './uploadCtrl';

export default function uploadAPI(dbService, fileStorageService) {
    const router = Router();
    const ctrl = new UploadCtrl(dbService, fileStorageService);

    /**
     *
     * @api {post} /api/upload Upload Image
     * @apiName UploadImage
     * @apiGroup Upload
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {File} image Uploaded image
     *
     * @apiSuccess (201) {String} _id Id
     * @apiSuccess (201) {String} location URL
     *
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "_id": "213fdsf32sads2323sss2",
     *       "location": "https://example.com/file_21312321.jpg"
     *     }
     *
     *
     */
    router.post('/', upload.single('image'), asyncHandler(ctrl.uploadImage));

    return router;
}
