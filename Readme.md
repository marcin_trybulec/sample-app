# Image Upload Application
### created by Marcin Trybulec
---------------------------------------------------
## Stack:
### Express, ES+, AWS S3, MongoDb, Mocha, Chai, Sinon, Supertest, Babel, ESLint, Prettier

---------------------------------
## To run:
You must provide your AWS Access Keys via environment variables: AWS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_BUCKET_NAME. I recommend creating an .env file in the root directory of the project. 
### Development:
> docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
### Test:
> docker-compose -f docker-compose.yml -f docker-compose.test.yml up
### Production:
> docker-compose -f docker-compose.yml -f docker-compose.prod.yml up