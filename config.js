const path = require('path');

require('dotenv').config();

const env = process.env.NODE_ENV || 'local';

const _default = {
    app: {
        port: normalizePort(process.env.APP_PORT) || 3000,
    },
    awsService: {
        accessKeyId: process.env.AWS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        bucketName: process.env.AWS_BUCKET_NAME,
    },
    mongoDb: {
        port: normalizePort(process.env.DB_PORT) || 27017,
    },
};

const development = {
    env: 'development',
    mongoDb: {
        host: process.env.DEV_DB_HOST || 'mongo',
        name: process.env.DEV_DB_NAME || 'db',
    },
};

const test = {
    env: 'test',
    mongoDb: {
        host: process.env.TEST_DB_HOST || 'mongo',
        name: process.env.TEST_DB_NAME || 'test',
    },
};

const production = {
    env: 'production',
    mongoDb: {
        host: process.env.DB_HOST || 'mongo',
        name: process.env.DB_NAME || 'db',
    },
};

const local = {
    env: 'local',
    mongoDb: {
        host: process.env.LOCAL_DB_HOST || 'localhost',
        name: process.env.LOCAL_DB_NAME || 'test',
    },
};

const config = {
    production,
    development,
    test,
    local,
};

module.exports = Object.assign({}, _default, config[env.trim()]);

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}
